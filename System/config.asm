;#######################################
;#                                     #
;#  CP/M Plus for Z80 Retro! 2063      #
;#         Configuration               #
;#                                     #
;# Most values in here should not be   #
;# changed, except for debugging or    #
;# development.                        #
;#######################################
;#    VDP:                             #
;# If you have the official VDP board, #
;# just set the "VDP" equate to "true".#
;# This will include the VDP console   #
;# driver in the build process and     #
;# make the VDP available as character #
;# device in CP/M. It can be enabled   #
;# by the DEVICE Tool, or in the ROM   #
;# Setup if using the SG-ROM Firmware. #
;#######################################

true		equ -1
false		equ not true

; enable for VDP
VDP			equ true
vdp			equ VDP
.debug_vdp	equ false
multimode 	equ false
VDP_font 	equ false

; Clear RAM-Disk on Init
rdclear		equ true

; IO port definitions
gpio_in		equ 000h	; GP input port
gpio_out	equ	010h	; GP output port
prn_stp		equ	000h	; Status bits input port
prn_ctrl	equ	010h	; Control output port
prn_dat		equ	020h	; printer data out
sioa_data	equ	030h	; SIO port A, data
siob_data	equ 031h	; SIO port B, data
sioa_ctrl	equ	032h	; SIO port A, control
siob_ctrl	equ	033h	; SIO port B, control
ctc_0		equ	040h	; CTC port 0
ctc_1		equ	041h	; CTC port 1
ctc_2		equ	042h	; CTC port 2
ctc_3		equ	043h	; CTC port 3
sioa_ctc	equ	ctc_1	; CTC port 1
siob_ctc	equ	ctc_2	; CTC port 2
vdp_vram	equ	080h	; VDP port for accessing the VRAM
vdp_reg		equ	081h	; VDP port for accessing the registers
joyport0	equ	0a8h	; I/O port for joystick 0
joyport1	equ	0a9h	; I/O port for joystick 1

; Status bit locations on the printer port
prn_err		equ	001h
prn_stat	equ	002h
prn_papr	equ	004h
prn_bsy		equ	008h
prn_ack		equ	010h
prn_stb		equ	008h

banked		equ true
.debug 		equ false
debug		equ	false

; BIOS3.Z80
m_debug		equ false
RTC_debug	equ false

; USER.Z80
.sysdebug 	equ false
.vdpdebug 	equ false
.joydebug	equ false

; SDMBR.Z80
.sd_debug 	equ false
.mbr_msg	equ false
caldebug	equ false
sddi		equ false
sdermmsg	equ false
sdretry		equ 4

; SDIMG.Z80
use_sdimg	equ true
sdidebug	equ false

; DUMP.Z80
incdump		equ debug | sdidebug | .sd_debug | caldebug | m_debug | RTC_debug | .debug

; Non-printable characters
cr			equ	0dh		; carriage return
lf			equ	0ah		; line feed
bell		equ 7

.sector_buffer equ 08000h

; WARNING:
; !! DO NOT PUT AN 'end' STATEMENT HERE !!
