; CP/M3 driver for 8M image drives
; for CP/M 2.2 compatibility

include config.asm

cseg

	public SDIMG, img_offset

	extrn sdinit, sdwrite, sdread
		
; Extern Variables
	extrn ?bank,gpiocache
	extrn @dtbl,@dma,@trk,@sect,@adrv,@rdrv,@cbnk,@dbnk
	extrn sd_scratch, sdstack, oldstack
	extrn sector_buf, buf_dirty, buf_sect, buf_track, .initialized

; Math routines
	extrn mul16,add32,cp16
	
; debug stuff
	extrn hexdump_a,puts_crlf,iputs
if sdidebug | debug
	extrn disk_dump,dump_regs,hexdmp,dumpdpb
endif

if banked
	DSEG
endif

    dw  img_write
    dw  img_read
    dw  img_login
    dw  img_init
    db  0           ; Unit. This will be passed back to us as @rdrv
    db  0           ; Type. can be used for arbitrary information
SDIMG:
    dw  0           ; XLT sector translation table (no xlation done)
    dw  0, 0, 0, 0  ; scratchpad
    db  0           ; scratch
    db  0           ; media change flag
    dw  dpb     	; DPB pointer
    dw  0FFFEh		; CSV will be created by GENCPM
    dw  0FFFEh      ; ALV pointer will be created by GENCPM
    dw  0FFFEh      ; DIRBCB pointer will be created by GENCPM
    dw  0FFFEh      ; DTABCB pointer will be created by GENCPM
    dw  0FFFEh      ; HASH determined by GENCPM
    dw  0FFFEh      ; HBANK determined by GENCPM

if banked
	CSEG
endif

img_offset:		
	dw 0000h,0008h
dpb:
	dw	64		; SPT (was 4x128, now 16x512)
	db	6		; (fixed) BSH
	db	63		; (fixed) BLM
	db	3		; (fixed) EXM
	dw	1021	; (fixed) DSM (max allocation block number)
	dw	511		; (fixed) DRM
	db	0C0h	; (fixed) AL0
	db	0		; (fixed) AL1
	dw	128		; (fixed) CKS (DRM/4)+1
	dw	2		; (fixed 16kB) OFF (was 32x(4x128), now 2x(16x512))
	db	0 		; PSH
	db	0		; PHM
	
img_init:
ret 
	
img_write:
	ld (oldstack),sp
	ld sp,sdstack
if sdidebug
	call iputs
	db 'IMG.WRITE',cr,lf,0
endif
	; check if we have the sector buffered
	ld bc,(@sect)
	ld a,c
	and 0FCh		; mask away the lowest 2 bits
	ld c,a
	ld de,(buf_sect)
	ld a,e
	and 0FCh		; mask away the lowest 2 bits
	ld e,a	
	call cp16
	jr nz,wrnobuf
	ld bc,(@trk)
	ld de,(buf_track)
	call cp16
	jr nz,wrnobuf
	jr wrgotbuf
wrnobuf:
	; buffer invalid
if sdidebug
	call iputs
	db 'IMG.WRITE.C.miss',cr,lf,0
endif
	call calcblock
	ld bc,sector_buf
if sddi
	di
endif
    call sdread        ; read the SD block
if sddi
	ei
endif
	or a
	jr nz,wrret
	ld bc,(@trk)
	ld (buf_track),bc
	ld bc,(@sect)
	ld (buf_sect),bc
	jr wrldir
	
wrgotbuf:
	; if we get here, buffer already has the data
if sdidebug
	call iputs
	db 'IMG.WRITE.C.hit',cr,lf,0
endif

wrldir:	
if banked
	ld a,(@dbnk)
	call ?bank
endif
	ld bc,(@sect)
	ld a,c
	and 03h	; get only the last 2 bits
	inc a
	ld b,a
	ld hl,sector_buf-128
	ld de,128
wradd:
	add hl,de
	djnz wradd

	ld de,(@dma)
	ex de,hl
	ld bc,128		; CPM record size

	ldir
	
if banked
	push af
	ld a,(@cbnk)
	call ?bank
	pop af
endif

	; Data has been copied into the sector buffer.
	; Now write it back
	call calcblock
	ld bc,sector_buf
if sddi
	di
endif
	call sdwrite
if sddi
	ei
endif

wrret:	
if sdidebug
	push af
	call iputs
	db '/IMG.WRITE:',0
	pop af
	push af
	call hexdump_a
	call puts_crlf
	call puts_crlf
	pop af
endif
	ld sp,(oldstack)
ret

img_read:
	ld (oldstack),sp
	ld sp,sdstack

if sdidebug
	call iputs
	db 'IMG.READ',cr,lf,0
endif
; check if we have the sector buffered
	ld bc,(@sect)
	ld a,c
	and 0FCh		; mask away the lowest 2 bits
	ld c,a
	ld de,(buf_sect)
	ld a,e
	and 0FCh		; mask away the lowest 2 bits
	ld e,a	
	call cp16
	jr nz,rdnobuf
	ld bc,(@trk)
	ld de,(buf_track)
	call cp16
	jr nz,rdnobuf
	; if we get here, buffer already has the data
if .sd_debug
	call iputs
	db 'IMG.READ.C.hit',cr,lf,0
endif
	jr rddone
rdnobuf:
	; buffer invalid
if .sd_debug
	call iputs
	db 'IMG.READ.C.miss',cr,lf,0
endif	
	call calcblock
	ld bc,sector_buf
if sddi
	di
endif
    call sdread        ; read the SD block
if sddi
	ei
endif
	or a
	jr nz,rdret
	ld bc,(@trk)
	ld (buf_track),bc
	ld bc,(@sect)
	ld (buf_sect),bc
	xor a
	ld (buf_dirty),a
	

rddone:
	
if banked
	ld a,(@dbnk)
	call ?bank
endif

	ld bc,(@sect)
	ld a,c
	and 03h	; get only the last 2 bits
	inc a
	ld b,a
	ld hl,sector_buf-128
	ld de,128
rdadd:
	add hl,de
	djnz rdadd

	ld bc,128		; CPM record size
	ld de,(@dma)
	ldir
	
	xor a
	
if sdidebug
	push af
	ld bc,32
	ld hl,(@dma)
	ld e,0
	call hexdmp ;dump BC bytes from HL. If E!=0 be fancy
	call puts_crlf
	pop af
endif

if banked
	push af
	ld a,(@cbnk)
	call ?bank
	pop af
endif
rdret:

if sdidebug
	push af
	call iputs
	db '/IMG.READ:',0
	pop af
	push af
	call hexdump_a
	call puts_crlf
	pop af
endif

	ld sp,(oldstack)
ret

img_login:
if sdidebug
	call iputs
	db 'IMG.LOGIN',cr,lf,0
endif	
ret
	
;##########################################################################
; Calculate the address of the SD block, given the CP/M track number
; in @trk, sector in @sect, and offset in the table at .drv_offset
; Return: the 32-bit block number in DE,HL
; Also deposit the address into the SD command buffer
; Based on proposal from Trevor Jacobs - 02-15-2023
;##########################################################################
	
calcblock:	
if sdidebug
	call iputs
	db 'IMG.CALC: ',0
	call disk_dump
endif
	
	ld ix,img_offset
	ld iy,sd_scratch+1
	; IX -> Pstart

	;ld	hl,0
	ld	bc,(@trk)		; BC = 16 (sectors/track)
	ld 	de,16			; DE = requested track	
						; note: flipped might be faster
if sdidebug
	call dump_regs
endif
						
	call mul16			; DE:HL = BC * DE

if sdidebug
	call dump_regs
endif

	ld bc,(@sect)		; BC = the requested sector
	; divide by 4
	srl b
	rr c
	srl b
	rr c
	add hl,bc			; DE:HL now the block offset

if sdidebug
	call dump_regs
endif

	call add32			; (IY) = DE:HL + (IX)

if sdidebug
	call dump_regs
	push iy
	pop hl
	ld bc,4
	ld e,0
	call hexdmp
	push ix
	pop hl
	ld bc,4
	ld e,0
	call hexdmp
	call puts_crlf
endif

    ld  d,(iy)            ; DE = low-word of the SD starting block
    ld  e,(iy+1)        ; DE = low-word of the SD starting block
    ld  h,(iy+2)      ; HL = high word of SD start block
    ld  l,(iy+3)        ; HL = high word of SD start block	

if sdidebug
	push hl
	push de
	call dump_regs
	call iputs
	db '/IMG.CALC',cr,lf,0
	pop de
	pop hl
endif
    ret

end
	