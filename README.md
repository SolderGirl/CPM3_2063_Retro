# CPM3_2063_Retro    

The CP/M 3 operating system, ported to John Winans' Z80 Retro board

Check out John's project at https://github.com/Z80-Retro
   
## WARNING   
 I have done a major rewrite of large parts of this code.   
 It has not been cleaned up yet.   
 Assembly may or may not fail at this point.   
    

## Building   
      
The sources contained in this repository are meant to be compiled on CP/M 3.
The files that originated from Digital Research are written in 8080 assembly, 
and are to be compiled with RMAC.

RMAC is part of the CP/M 3 distribution package, available from http://www.cpm.z80.de/

The sources written by me, and the parts taken from John's code are written 
in Z80 assembly and compile with ZMAC. I will include the ZMAC.COM binary 
here, because i could not find a source online.

For building the sources, I use the Altair Z80 Simulator from 
http://www.s100computers.com/Software%20Folder/Altair%20Simmulator/Altair%20Software.htm

For testing and debugging, I use this awesome emulator:
https://github.com/EtchedPixels/EmulatorKit
     
     
For building the binaries, you need a running CP/M system with the programs   
MAKE.COM  
ZMAC.COM   
RMAC.COM   
LINK.COM   
The latter two are part of the original CP/M 3 distribution. The other two can be found on the famous OAK CDROM.   
The easiest way to transfer CPM3SG.LBR to your CP/M system and unpack it there.   
To build everything, CPM3.SYS, the bootloader, the ROM and all the tools, simply enter "MAKE" at the console. After a lengthy build process, you will have everything ready.   
Other targets for MAKE include:   
MAKE clean   
Delete intermediate files used during the build process   
   
MAKE fresh   
Like MAKE clean, but also delete the final build binaries.   
   
MAKE cpm3.sys/rom.bin/ldr.bin/*.com   
Build every file individually. Useful if you changed just one part and don't want to build everything. If you changed a file but MAKE claims it is up to date, you have to run MAKE clean or MAKE fresh.
   
   
   

All the manuals for CP/M 3 are available from here: http://www.s100computers.com/Software%20Folder/CPM3%20BIOS%20Installation/CPM3%20FLOPPY%20BIOS%20Software.htm

For using the System, I reccomend reading the User Guide and keeping the Command Guide handy for reference.
If you want to write programs to run on CP/M 3, read the Programmers Guide.
The System Guide is meant for those who port CP/M 3 to a new system. But it is also a great source to learn about the inner workings of CP/M 3.
